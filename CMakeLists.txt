cmake_minimum_required(VERSION 3.0.0)
project(vx4718_test VERSION 0.1.0 LANGUAGES C CXX)
set(CMAKE_VERBOSE_MAKEFILE ON)
include(CTest)
enable_testing()

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_compile_options(-fsanitize=address)
add_link_options(-fsanitize=address)

add_executable(vx4718_test main.cpp)
target_link_libraries(vx4718_test /usr/lib/libCAENVME.so)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
