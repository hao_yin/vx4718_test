#include <iostream>
#include <chrono>
#include <thread>
#include <vector>

#include <CAENVMElib.h>

static constexpr auto IP_ADDR = "192.168.202.69";

static constexpr auto FADC_WRITE = uint32_t{0x41050000u};
static constexpr auto FADC_READ = uint32_t{0x41070000u};
static constexpr auto FADC_BLOCK_WRITE = uint32_t{0x41060000u};
static constexpr auto FADC_BLOCK_READ = uint32_t{0x41080000u};

auto test_fadc_single_transfer(int handle, uint32_t value) -> bool
{
    auto ret = CAENVME_WriteCycle(handle, FADC_WRITE,
                                  &value,
                                  CVAddressModifier::cvA32_U_DATA,
                                  CVDataWidth::cvD32);
    if (ret != cvSuccess)
    {
        std::cerr << "Failed to write: " << CAENVME_DecodeError(ret);
        return false;
    }

    auto readback = uint32_t{0};
    ret = CAENVME_ReadCycle(handle,
                            FADC_READ,
                            &readback,
                            CVAddressModifier::cvA32_U_DATA,
                            CVDataWidth::cvD32);
    if (ret != cvSuccess)
    {
        std::cerr << "Failed to readback:" << CAENVME_DecodeError(ret);
        return false;
    }

    std::cout << "Expected: " << value << ", got: " << readback << '\n';
    return readback == value;
}

auto test_fadc_block_transfer(int handle, const std::vector<uint32_t> &data) -> bool
{
    auto transfered = 0;
    auto ret = CAENVME_FIFOBLTWriteCycle(handle,
                                         FADC_BLOCK_WRITE,
                                         data.data(),
                                         data.size() * sizeof(uint32_t),
                                         CVAddressModifier::cvA32_U_BLT,
                                         CVDataWidth::cvD32,
                                         &transfered);
    std::cout << "data written = " << transfered / sizeof(uint32_t)
              << ", err code = " << CAENVME_DecodeError(ret) << '\n';

    auto readback = std::vector<uint32_t>(data.size(), uint32_t{0});
    ret = CAENVME_FIFOBLTReadCycle(handle, FADC_BLOCK_READ,
                                   readback.data(),
                                   readback.size() * sizeof(uint32_t),
                                   CVAddressModifier::cvA32_U_BLT, CVDataWidth::cvD32,
                                   &transfered);
    std::cout << "transfered: " << transfered / sizeof(uint32_t)
              << "\nerr code: " << CAENVME_DecodeError(ret) << '\n';

    for (auto i = 0ul; i < data.size(); ++i)
        std::cout << '\t' << data[i] << "  " << readback[i] << '\n';

    return true;
}

auto main() -> int
{
    auto block_data = std::vector<uint32_t>(size_t{8}, uint32_t{0xaa});
    auto i = 0;
    for (auto &val : block_data)
        val = i++;

    auto handle = -1;
    // auto ret = CAENVME_Init2(CVBoardTypes::cvETH_V4718, IP_ADDR, 0, &handle);
    const auto LINK = 0;
    auto ret = CAENVME_Init2(CVBoardTypes::cvPCIE_A3818_V4718, &LINK, 0, &handle);
    if (ret != cvSuccess)
    {
        std::cerr << CAENVME_DecodeError(ret) << '\n';
        std::cout << "End ret:" << CAENVME_End(handle) << '\n';
        return 1;
    }

    for (auto i = uint32_t{0}; i < 10; ++i)
    {
        if (!test_fadc_single_transfer(handle, i))
        {
            std::cout << "End ret:" << CAENVME_End(handle) << '\n';
            return 1;
        }
        if (!test_fadc_block_transfer(handle, block_data))
        {
            std::cout << "End ret:" << CAENVME_End(handle) << '\n';
            return 1;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    std::cout << "End ret:" << CAENVME_End(handle) << '\n';
}
