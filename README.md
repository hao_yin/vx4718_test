# Simple sanity check

1. Single transfer:
    1. write value to a test register
    2. readback the value and assert equality
2. Block transfer (FIFOed):
    1. write 8 uint32_t to a test address
    2. readback all 8 values


NOTE: Enabled address sanitizer by default, since we also observed potential
mem issues. One can disable asan in CMakeLists.txt by commenting out the
following:

```cmake
add_compile_options(-fsanitize=address)
add_link_options(-fsanitize=address)
```

NOTE: we have custom board used to test the transfer. The addr are defined
in main.cpp!

```
static constexpr auto FADC_WRITE = uint32_t{0x41050000u};
static constexpr auto FADC_READ = uint32_t{0x41070000u};
static constexpr auto FADC_BLOCK_WRITE = uint32_t{0x41060000u};
static constexpr auto FADC_BLOCK_READ = uint32_t{0x41080000u};
```


# Build and Run

```
mkdir build
cmake -S . -B build
cmake --build build --config Debug
./build/vx4718_test
```

# To switch between Ethernet and PCI

Please comment out 

```cpp
auto ret = CAENVME_Init2(CVBoardTypes::cvETH_V4718, IP_ADDR, 0, &handle);
```

and comment in
```cpp
// const auto LINK = 0;
// auto ret = CAENVME_Init2(CVBoardTypes::cvPCIE_A3818_V4718, &LINK, 0, &handle);
```

in main.cpp!

# Output with A3818 -> VX4718

Expected output with A3818 -> VX4718:
```
hyin@hyin-ts-p520:~/test_vx4718/build master
> ./vx4718_test 
Expected: 0, got: 0
data written = 8, err code = Success
transfered: 8
err code: Success
        0  0
        1  1
        2  2
        3  3
        4  4
        5  5
        6  6
        7  7
Expected: 1, got: 1
data written = 8, err code = Success
transfered: 8
err code: Success
        0  0
        1  1
        2  2
        3  3
        4  4
        5  5
        6  6
        7  7
Expected: 2, got: 2
data written = 8, err code = Success
transfered: 8
err code: Success
        0  0
        1  1
        2  2
        3  3
        4  4
        5  5
        6  6
        7  7
Expected: 3, got: 3
data written = 8, err code = Success
transfered: 8
err code: Success
        0  0
        1  1
        2  2
        3  3
        4  4
        5  5
        6  6
        7  7
Expected: 4, got: 4
data written = 8, err code = Success
transfered: 8
err code: Success
        0  0
        1  1
        2  2
        3  3
        4  4
        5  5
        6  6
        7  7
Expected: 5, got: 5
data written = 8, err code = Success
transfered: 8
err code: Success
        0  0
        1  1
        2  2
        3  3
        4  4
        5  5
        6  6
        7  7
Expected: 6, got: 6
data written = 8, err code = Success
transfered: 8
err code: Success
        0  0
        1  1
        2  2
        3  3
        4  4
        5  5
        6  6
        7  7
Expected: 7, got: 7
data written = 8, err code = Success
transfered: 8
err code: Success
        0  0
        1  1
        2  2
        3  3
        4  4
        5  5
        6  6
        7  7
Expected: 8, got: 8
data written = 8, err code = Success
transfered: 8
err code: Success
        0  0
        1  1
        2  2
        3  3
        4  4
        5  5
        6  6
        7  7
Expected: 9, got: 9
data written = 8, err code = Success
transfered: 8
err code: Success
        0  0
        1  1
        2  2
        3  3
        4  4
        5  5
        6  6
        7  7
End ret:0
```

NOTE: no warning from address sanitizer!


# Output with Ethernet -> VX4718

```cpp
hyin@hyin-ts-p520:~/test_vx4718/build master
> ./vx4718_test 
Expected: 0, got: 0
data written = 0, err code = Timeout
transfered: 1
err code: Success
        0  8
        1  0
        2  0
        3  0
        4  0
        5  0
        6  0
        7  0
Failed to readback:Communication errorEnd ret:0

=================================================================
==66038==ERROR: LeakSanitizer: detected memory leaks

Direct leak of 16777232 byte(s) in 1 object(s) allocated from:
    #0 0x7f0d4ba7e887 in __interceptor_malloc ../../../../src/libsanitizer/asan/asan_malloc_linux.cpp:145
    #1 0x7f0d4b60f774  (/usr/lib/libCAENVME.so+0xf774)

SUMMARY: AddressSanitizer: 16777232 byte(s) leaked in 1 allocation(s).
```

**NOTE: need to clarify if the above mem leak if a false positive!** - Could be caused by not having a version of the lib. compiled with asan... but in this ERROR must also be visible with A3818.

Apart from the main issues!

# Output with Ethernet -> VX4718 only block transfers

Note:

```cpp
if (!test_fadc_single_transfer(handle, i))
{
    std::cout << "End ret:" << CAENVME_End(handle) << '\n';
    return 1;
}
```

The above had been commented out. to check the data after a fifoed block write!

```
hyin@hyin-ts-p520:~/test_vx4718/build master
> ./vx4718_test 
data written = 0, err code = Timeout
transfered: 1
err code: Success
        0  0
        1  0
        2  0
        3  0
        4  0
        5  0
        6  0
        7  0
data written = 0, err code = Timeout
transfered: 1
err code: Success
        0  8
        1  0
        2  0
        3  0
        4  0
        5  0
        6  0
        7  0
data written = 0, err code = Timeout
transfered: 8
err code: Success
        0  0
        1  1
        2  2
        3  3
        4  4
        5  5
        6  6
        7  7
data written = 0, err code = Timeout
transfered: 1
err code: Success
        0  8
        1  0
        2  0
        3  0
        4  0
        5  0
        6  0
        7  0
data written = 0, err code = Timeout
transfered: 8
err code: Success
        0  0
        1  1
        2  2
        3  3
        4  4
        5  5
        6  6
        7  7
data written = 0, err code = Timeout
transfered: 1
err code: Success
        0  8
        1  0
        2  0
        3  0
        4  0
        5  0
        6  0
        7  0
data written = 0, err code = Timeout
transfered: 8
err code: Success
        0  0
        1  1
        2  2
        3  3
        4  4
        5  5
        6  6
        7  7
data written = 0, err code = Timeout
transfered: 1
err code: Success
        0  8
        1  0
        2  0
        3  0
        4  0
        5  0
        6  0
        7  0
data written = 0, err code = Timeout
transfered: 8
err code: Success
        0  0
        1  1
        2  2
        3  3
        4  4
        5  5
        6  6
        7  7
data written = 0, err code = Timeout
transfered: 1
err code: Success
        0  8
        1  0
        2  0
        3  0
        4  0
        5  0
        6  0
        7  0
End ret:0

=================================================================
==68182==ERROR: LeakSanitizer: detected memory leaks

Direct leak of 16777232 byte(s) in 1 object(s) allocated from:
    #0 0x7f9c3317f887 in __interceptor_malloc ../../../../src/libsanitizer/asan/asan_malloc_linux.cpp:145
    #1 0x7f9c32e0f774  (/usr/lib/libCAENVME.so+0xf774)

SUMMARY: AddressSanitizer: 16777232 byte(s) leaked in 1 allocation(s).
```

Appart from the first readback, we see expected data every 2nd access, while the other accesses shows size of transfered data in the 0th element (8)!

Potential Mem leak still there.
